<div class="form-group col-md-12">
    <h1 style="text-align: center; font-size:20px; text-transform: uppercase; font-style: italic">Dados do cliente</h1>
    <hr>
</div>

<div class="form-group col-md-4">
    <label>Nome</label>
    <input type="text" class="form-control{{$errors->has('name') ? ' is-invalid ': ''}}" value="{{old('name') ?? ($register->client->name ?? '')}}" id="name" name="name">
    <input type="hidden" id="client_id" name="client_id">
    @validation(['errors' => $errors->has('name'), 'errors_message' => $errors->first('name')])
    @endvalidation
</div>
<div class="form-group col-md-4">
    <label>E-mail</label>
    <input type="email" class="form-control{{$errors->has('email') ? ' is-invalid ': ''}}" name="email" value="{{old('email') ?? ($register->client->email ?? '')}}" id="email" readonly>
    @validation(['errors' => $errors->has('email'), 'errors_message' => $errors->first('email')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>Telefone</label>
    <input type="text" class="form-control{{$errors->has('phone') ? ' is-invalid ': ''}}" name="phone" value="{{old('phone') ?? ($register->client->phone ?? '')}}" id="phone" readonly>
    @validation(['errors' => $errors->has('phone'), 'errors_message' => $errors->first('phone')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>CEP</label>
    <input type="text" class="form-control{{$errors->has('cep') ? ' is-invalid ': ''}}" name="cep" value="{{old('cep') ?? ($register->client->cep ?? '')}}" id="cep" >
    @validation(['errors' => $errors->has('cep'), 'errors_message' => $errors->first('cep')])
    @endvalidation
</div>
<div class="form-group col-md-5">
    <label>Endereço</label>
    <input type="text" class="form-control{{$errors->has('street') ? ' is-invalid ': ''}}" name="street" value="{{old('street') ?? ($register->client->street ?? '')}}" id="street" readonly>
    @validation(['errors' => $errors->has('street'), 'errors_message' => $errors->first('street')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>Número</label>
    <input type="text" class="form-control{{$errors->has('number') ? ' is-invalid ': ''}}" name="number" value="{{old('number') ?? ($register->client->number ?? '')}}" id="number" readonly>
    @validation(['errors' => $errors->has('number'), 'errors_message' => $errors->first('number')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>Bairro</label>
    <input type="email" class="form-control{{$errors->has('neighborhood') ? ' is-invalid ': ''}}" name="neighborhood" value="{{old('neighborhood') ?? ($register->client->neighborhood ?? '')}}" id="neighborhood" readonly>
    @validation(['errors' => $errors->has('neighborhood'), 'errors_message' => $errors->first('neighborhood')])
    @endvalidation
</div>
<div class="form-group col-md-3">
    <label>Cidade</label>
    <input type="text" class="form-control{{$errors->has('city') ? ' is-invalid ': ''}}" name="city" value="{{old('city') ?? ($register->client->city ?? '')}}" id="city" readonly>
    @validation(['errors' => $errors->has('city'), 'errors_message' => $errors->first('city')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>UF</label>
    <input type="text" class="form-control{{$errors->has('uf') ? ' is-invalid ': ''}}" value="{{old('uf') ?? ($register->client->uf ?? '')}}" id="uf" name="uf" readonly>
    @validation(['errors' => $errors->has('uf'), 'errors_message' => $errors->first('uf')])
    @endvalidation
</div>
<div class="col-md-12">
    <hr>
</div>
<div class="form-group col-md-12">
    <h1 style="text-align: center; font-size:20px; text-transform: uppercase; font-style: italic">Dados do Produto</h1>
    <hr>
</div>

<div class="form-group col-md-4">
    <label>Nome</label>
    <input type="text" class="form-control{{$errors->has('name_prod') ? ' is-invalid ': ''}}" value="{{old('name_prod') ?? ($register->product->name ?? '')}}" id="name_prod" name="name_prod">
    <input type="hidden" id="product_id" name="product_id">
    @validation(['errors' => $errors->has('name_prod'), 'errors_message' => $errors->first('name_prod')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>Referência</label>
    <input type="text" class="form-control{{$errors->has('reference') ? ' is-invalid ': ''}}" value="{{old('reference') ?? ($register->product->reference ?? '')}}" id="reference" name="reference" readonly>
</div>
<div class="form-group col-md-2">
    <label>Data da Compra</label>
    <input type="date" class="form-control{{$errors->has('date_buy') ? ' is-invalid ': ''}}" value="{{old('date_buy') ?? ($register->date_buy ?? '')}}" id="date_buy" name="date_buy">
    @validation(['errors' => $errors->has('date_buy'), 'errors_message' => $errors->first('date_buy')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>Garantia</label>
    <input type="text" class="form-control" value="{{old('warranty')}}" id="warranty" name="warranty" readonly>
</div>
<div class="form-group col-md-2">
    <label>Tensão</label>
    <input type="text" class="form-control{{$errors->has('tension') ? ' is-invalid ': ''}}" value="{{old('tension') ?? ($register->tension ?? '')}}" id="tension" name="tension" readonly>
    @validation(['errors' => $errors->has('tension'), 'errors_message' => $errors->first('tension')])
    @endvalidation
</div>
<div class="form-group col-md-12">
    <label>Descrição</label>
    <textarea class="form-control" id="description_prod" name="description_prod" rows="3" readonly><?php echo e(old('description_prod') ?? $register->description_prod ?? ''); ?></textarea>
    <?php $__env->startComponent('components.validation', ['errors' => $errors->has('description_prod'), 'errors_message' => $errors->first('description_prod')]); ?>
    <?php echo $__env->renderComponent(); ?>
</div>

<div class="form-group col-md-12">
    <h1 style="text-align: center; font-size:20px; text-transform: uppercase; font-style: italic">Funcionário Responsável</h1>
    <hr>
</div>

<div class="form-group col-md-8">
    <label>Nome</label>

</div>



<div class="form-group col-md-12">
    <h1 style="text-align: center; font-size:20px; text-transform: uppercase; font-style: italic">Serviço Prestado</h1>
    <hr>
</div>

<div class="form-group col-md-6">
    <label>Problema</label>
    <input type="text" class="form-control{{$errors->has('problem') ? ' is-invalid ': ''}}" value="{{old('problem') ?? ($register->problem ?? '')}}" id="problem" name="problem" >
    @validation(['errors' => $errors->has('problem'), 'errors_message' => $errors->first('problem')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>Ferramenta utilizada</label>
    <select class="form-control" name="item" id="item">
        @foreach($itens as $item)
            <option value="{{$item->id}}" {{isset($register) && $register->iten_id == $item->id ? "selected" : null}}>{{$item->name}}</option>
            <option value="{{$item->id}}" {{isset($register) && $register->iten_id == $item->id ? "selected" : null}}>{{$item->name}}</option>
        @endforeach
    </select>
</div>
<div class="form-group col-md-2">
    <label>Número NFE</label>
    <input type="number" class="form-control{{$errors->has('nfe') ? ' is-invalid ': ''}}" value="{{old('nfe') ?? ($register->nfe ?? '')}}" id="nfe" name="nfe">
    @validation(['errors' => $errors->has('nfe'), 'errors_message' => $errors->first('nfe')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>Serviço Prestado</label>
    <p>&nbsp;</p>
</div>
<div class="form-group col-md-2">
    <label>Valor mão de obra</label>
    <p>&nbsp;</p>
</div>
<div class="form-group col-md-2">
    <label>Valor cobrado a parte</label>
    <input type="text" class="form-control{{$errors->has('value_more') ? ' is-invalid ': ''}}" value="{{(old('value_more') ?? $register->value_more ?? "0,00")}}" id="value_more" name="value_more">
    @validation(['errors' => $errors->has('value_more'), 'errors_message' => $errors->first('value_more')])
    @endvalidation
</div>
<div class="form-group col-md-3">
    <label>Ínicio do serviço</label>
    <input type="date" class="form-control{{$errors->has('service_start') ? ' is-invalid ': ''}}" value="{{old('service_start') ??($register->service_start ?? '')}}" id="service_start" name="service_start">
    @validation(['errors' => $errors->has('service_start'), 'errors_message' => $errors->first('service_start')])
    @endvalidation
</div>
<div class="form-group col-md-3">
    <label>Fim do serviço</label>
    <input type="date" class="form-control{{$errors->has('service_finish') ? ' is-invalid ': ''}}" value="{{old('service_finish') ?? ($register->service_finish ?? '')}}" id="service_finish" name="service_finish">
    @validation(['errors' => $errors->has('service_finish'), 'errors_message' => $errors->first('service_finish')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>Valor total da nota</label>
    <input type="text" class="form-control" value="0.00" id="amount" name="amount" readonly>
    <p>&nbsp;</p>
</div>
<div class="form-group col-md-12">
    <label>Descrição</label>
    <textarea class="form-control{{$errors->has('description') ? ' is-invalid ': ''}}" id="description" name="description" rows="3">{{($register->description ?? '')}}</textarea>
    @validation(['errors' => $errors->has('description'), 'errors_message' => $errors->first('description')])
    @endvalidation
</div>


