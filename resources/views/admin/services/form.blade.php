<div class="form-group col-md-4">
    <label>Nome</label>
    <input type="text" class="form-control{{$errors->has('name') ? ' is-invalid ' : ''}}" value="{{old('name') ?? $register->name ?? ''}}" name="name" id="name" placeholder="Digite o nome" >
    @validation(['errors' => $errors->has('name'), 'errors_message' => $errors->first('name')])
    @endvalidation
</div>
<div class="form-group col-md-4">
    <label>Valor Diária</label>
    <input type="text" maxlength="7" class="form-control{{$errors->has('diaria') ? ' is-invalid ' : ''}}" value="{{old('diaria') ?? $register->value ?? ''}}" name="diaria" id="diaria" placeholder="R$" >
    @validation(['errors' => $errors->has('diaria'), 'errors_message' => $errors->first('diaria')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>Garantia</label>
    <select class="form-control" name="warranty_number" id="warranty_number" >
        @for($i=1;$i<=12;$i++)
            <option value="{{$i}}" {{isset($register) && $register->warranty_number == $i ? "selected" : ''}}>{{$i}}</option>
        @endfor
    </select>
</div>
<div class="form-group col-md-2">
    <label>&nbsp;</label>
    <select class="form-control" name="warranty_string" id="warranty_string" >
        <option value="Mês(es)" {{isset($register) && $register->warranty_string == "Mês(es)" ? "selected" : ''}}>Mês(es)</option>
        <option value="Ano(s)" {{isset($register) && $register->warranty_string == "Ano(s)" ? "selected" : ''}}>Ano(s)</option>
    </select>
</div>
<div class="form-group col-md-12">
    <label>Descrição</label>
    <textarea class="form-control{{$errors->has('name') ? ' is-invalid ' : ''}}" id="description" name="description" rows="3">{{$register->description ?? ''}}</textarea>
    @validation(['errors' => $errors->has('description'), 'errors_message' => $errors->first('description')])
    @endvalidation
</div>
