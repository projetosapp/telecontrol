<head>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
</head>
<div class="container">
    <div class="row">
        <div class="col-xs-2 col-xs-2">
            <b>Código</b>
            <p>{{$service->id}}</p>
        </div>
        <div class="col-xs-2 col-xs-2">
            <b>Inicializado Em</b>
            <p>{!! $service->service_start !!}</p>
        </div>
        <div class="col-xs-2 col-xs-2">
            <b>Finalizado Em</b>
            <p>{{$service->service_finish}}</p>
        </div>
    </div>
    <div class="col-md-12">
        <hr>
        <h1 style="text-align: center; text-transform: uppercase; font-style: italic; font-size: 15px">Dados do cliente</h1>
        <hr>
    </div>
    <div class="row">
        <div class="col-xs-2 col-xs-2">
            <b>Nome</b>
            <p>{{$service->client->name}}</p>
        </div>
        <div class="col-xs-4 col-xs-4">
            <b>E-mail</b>
            <p>{{$service->client->email}}</p>
        </div>
        <div class="col-xs-2 col-xs-2">
            <b>Telefone</b>
            <p>{{$service->client->phone}}</p>
        </div>
        <div class="col-xs-2 col-xs-2">
            <b>CEP</b>
            <p>{{$service->client->cep}}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4 col-xs-4">
            <b>Rua</b>
            <p>{{$service->client->street}}</p>
        </div>
        <div class="col-xs-2 col-xs-2">
            <b>Número</b>
            <p>{{$service->client->number}}</p>
        </div>
        <div class="col-xs-3 col-xs-3">
            <b>Bairro</b>
            <p>{{$service->client->neighborhood}}</p>
        </div>
        <div class="col-xs-3">
            <b>Cidade</b>
            <p>{{$service->client->city.','.$service->client->uf}}</p>
        </div>
    </div>
    <div class="col-md-12">
        <hr>
        <h1 style="text-align: center; text-transform: uppercase; font-style: italic; font-size: 15px">Dados do Produto</h1>
        <hr>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <b>Nome</b>
            <p>{{$service->product->name}}</p>
        </div>
        <div class="col-xs-3">
            <b>Referência</b>
            <p>{{$service->product->reference}}</p>
        </div>
        <div class="col-xs-2">
            <b>Garantia</b>
            <p>{{$service->warranty}}</p>
        </div>
        <div class="col-xs-2">
            <b>Tensão</b>
            <p>{{$service->product->tension}}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <b>Descrição</b>
            <p>{{$service->product->description}}</p>
        </div>
    </div>
    <div class="col-md-12">
        <hr>
        <h1 style="text-align: center; text-transform: uppercase; font-style: italic; font-size: 15px">Funcionario Responsável</h1>
        <hr>
    </div>
    <div class="row col-md-12">
        <div class="col-xs-4">
            <b>Nome</b>
            <p>{{$service->collaborator->user->name}}</p>
        </div>
        <div class="col-xs-3">
            <b>Área de Atuação</b>
            <p>{{$service->service->name}}</p>
        </div>
    </div>
    <div class="col-md-12">
        <hr>
        <h1 style="text-align: center; text-transform: uppercase; font-style: italic; font-size: 15px">Serviço Prestado</h1>
        <hr>
    </div>
    <div class="row">
        <div class="col-xs-5">
            <b>Problema</b>
            <p>{{$service->problem}}</p>
        </div>
        <div class="col-xs-3">
            <b>Ferramenta Últilizada</b>
            <p>{{$service->item->name}}</p>
        </div>
        <div class="col-xs-2">
            <b>Nota Fiscal</b>
            <p>{{$service->nfe}}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-5">
            <b>Serviço prestado</b>
            <p>{{$service->service->name}}</p>
        </div>
        <div class="col-xs-2">
            <b>Valor</b>
            <p>{{$service->value_service}}</p>
        </div>
        <div class="col-xs-2">
            <b>Valor Adicional</b>
            <p>{{$service->value_more}}</p>
        </div>
        <div class="col-xs-2">
            <b>Total</b>
            <p>{{$service->amount}}</p>
        </div>
    </div>
</div>
