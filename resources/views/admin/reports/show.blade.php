@extends('adminlte::page')

@section('content_header')
    <div class="headerDashboard">
        <h1>Ordem de Serviço N° {{$order_service->id}}</h1>
    </div>
@stop

@section('css')
    <style>
        .content-wrapper{
            height: 1100px;
        }
    </style>
@stop

@section('content')
    @page_component(['col' => 12])

    <div class="col-md-12">
        <a href="{{route('pdf-service-admin', $order_service->id)}}"><button class="btn btn-info btn-circle btn-xl" style="float: right" title="Gerar PDF"><i class="fas fa-file-pdf"></i></button></a>
    </div>
    @include('admin.reports.list-service')

    @endpage_component
@stop
