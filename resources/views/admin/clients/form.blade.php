<div class="form-group col-md-4">
    <label>Nomes</label>
    <input type="text" class="form-control{{$errors->has('name') ? ' is-invalid ': ''}}" value="{{old('name') ?? ($register->name ?? '')}}" id="name" name="name" placeholder="Digite o nome">
    @validation(['errors' => $errors->has('name'), 'errors_message' => $errors->first('name')])
    @endvalidation
</div>
<div class="form-group col-md-4">
    <label>E-mail</label>
    <input type="email" class="form-control{{$errors->has('email') ? ' is-invalid ': ''}}" value="{{old('email') ?? ($register->email ?? '')}}" id="email" name="email" placeholder="Digite seu melhor E-mail">
    @validation(['errors' => $errors->has('email'), 'errors_message' => $errors->first('email')])
    @endvalidation
</div>
<div class="form-group col-md-3">
    <label>Telefone</label>
    <input type="text" class="form-control{{$errors->has('phone') ? ' is-invalid ': ''}}" value="{{old('phone') ?? ($register->phone ?? '')}}" name="phone" placeholder="Digite o telefone">
    @validation(['errors' => $errors->has('phone'), 'errors_message' => $errors->first('phone')])
    @endvalidation
</div>
<div class="form-group col-md-3">
    <label>Seguimento</label>
    <input type="text" class="form-control{{$errors->has('following') ? ' is-invalid ': ''}}" value="{{old('following') ?? ($register->following ?? '')}}" id="following" name="following" placeholder="Digite o seguimento da empresa">
    @validation(['errors' => $errors->has('following'), 'errors_message' => $errors->first('following')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>CEP</label>
    <input type="text" class="form-control{{$errors->has('cep') ? ' is-invalid ': ''}}" value="{{old('cep') ?? ($register->cep ?? '')}}" id="cep" name="cep" placeholder="Digite seu CEP">
    @validation(['errors' => $errors->has('cep'), 'errors_message' => $errors->first('cep')])
    @endvalidation
</div>
<div class="form-group col-md-4">
    <label>Endereço</label>
    <input type="street" class="form-control" id="street" name="street" placeholder="Endereço" value="{{($register->street ?? '')}}" readonly>
    <small>&nbsp;</small>
</div>
<div class="form-group col-md-2">
    <label>N°</label>
    <input type="text" class="form-control{{$errors->has('number') ? ' is-invalid ': ''}}" value="{{old('number') ?? ($register->number ?? '')}}" id="number" name="number" placeholder="Número">
    @validation(['errors' => $errors->has('number'), 'errors_message' => $errors->first('number')])
    @endvalidation
</div>
<div class="form-group col-md-3">
    <label>Bairro</label>
    <input type="text" class="form-control" id="neighborhood" name="neighborhood" value="{{($register->neighborhood ?? '')}}" placeholder="Bairro" readonly>
</div>
<div class="form-group col-md-3">
    <label>Cidade</label>
    <input type="text" class="form-control" id="city" name="city" value="{{($register->city ?? '')}}" placeholder="Cidade" readonly>
</div>
<div class="form-group col-md-2">
    <label>UF</label>
    <input type="text" class="form-control" id="uf" name="uf" value="{{($register->uf ?? '')}}" placeholder="UF" readonly>
</div>
