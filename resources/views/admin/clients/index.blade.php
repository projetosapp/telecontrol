@extends('adminlte::page')

@section('content_header')
    <div class="headerDashboard">
        <h1>Clientes</h1>
    </div>
@stop

@section('content')
    @page_component(['col' => 12])

    @header_page(['routeName' => $routeName, 'search' => $search])
    @endheader_page

    @table(['columnList' => $columnList, 'list' => $list, 'routeName' => $routeName])
    @endtable

    @paginate(['search' => $search, 'list' => $list])
    @endpaginate

    @alert(['msg' => session('msg'), 'status' => session('status')])
    @endalert

    @endpage_component
@stop
