<div class="form-group col-md-4">
    <label>Nome</label>
    <input type="text" class="form-control{{$errors->has('name') ? ' is-invalid ': ''}}" value="{{old('name') ?? ($register->user->name ?? '')}}" id="name" name="name" placeholder="Digite o nome">
    @validation(['errors' => $errors->has('name'), 'errors_message' => $errors->first('name')])
    @endvalidation
</div>
<div class="form-group col-md-4">
    <label>E-mail</label>
    <input type="email" class="form-control{{$errors->has('email') ? ' is-invalid ': ''}}" name="email" value="{{old('email') ?? ($register->user->email ?? '')}}" id="email" placeholder="Digite seu melhor E-mail" >
    @validation(['errors' => $errors->has('email'), 'errors_message' => $errors->first('email')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>Função</label>
    <select class="form-control" id="function" name="function" >
        @foreach($data_services as $item)
            <option value="{{$item->id}}" {{(isset($register) && $register->service_id == $item->id) ? "selected" : ''}}>{{$item->name}}</option>
        @endforeach
    </select>
</div>
<div class="form-group col-md-2">
    <label>CEP</label>
    <input type="text" class="form-control{{$errors->has('cep') ? ' is-invalid ': ''}}" value="{{old('cep') ?? ($register->cep ?? '')}}" name="cep" id="cep" placeholder="Digite seu CEP" >
    @validation(['errors' => $errors->has('cep'), 'errors_message' => $errors->first('cep')])
    @endvalidation
</div>
<div class="form-group col-md-6">
    <label>Endereço</label>
    <input type="text" class="form-control" name="street" id="street" value="{{($register->street ?? '')}}" placeholder="Endereço" readonly>
</div>
<div class="form-group col-md-2">
    <label>N°</label>
    <input type="text" class="form-control{{$errors->has('cep') ? ' is-invalid ': ''}}" value="{{old('number') ?? ($register->number ?? '')}}" name="number" id="number" placeholder="Número">
    @validation(['errors' => $errors->has('number'), 'errors_message' => $errors->first('number')])
    @endvalidation
</div>
<div class="form-group col-md-4">
    <label>Bairro</label>
    <input type="text" class="form-control" name="neighborhood" value="{{($register->neighborhood ?? '')}}" id="neighborhood" placeholder="Bairro" readonly>
    <p>&nbsp;</p>

</div>
<div class="form-group col-md-3">
    <label>Cidade</label>
    <input type="text" class="form-control" name="city" id="city" value="{{($register->city ?? '')}}" placeholder="Cidade" readonly>
</div>
<div class="form-group col-md-2">
    <label>UF</label>
    <input type="text" class="form-control" name="uf" id="uf" value="{{($register->uf ?? '')}}" placeholder="UF" readonly>
</div>
