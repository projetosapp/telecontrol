@extends('adminlte::page')

@section('content_header')
    <div class="headerDashboard">
        <h1>Editar Colaborador</h1>
    </div>
@stop

@section('content')

    @page_component(['col' => 12])

    @alert(['msg' => session('msg'), 'status' => session('status')])
    @endalert

    @button_back(['routeName' => $routeName])
    @endbutton_back

    @form_component(['action' => route($routeName.".update", $register->id), 'method' => "PUT"])
    @include('admin.collaborators.form')
    <div class="form-group col-md-12">
        <button type="submit" class="btn btn-success btn-circle btn-xl marginTop" title="Cadastrar" style="float: right"><i class="fa fa-save"></i></button>
    </div>
    @endform_component

    @endpage_component

@stop

@section('js')
    <script src="{{ asset('js/buscaCep.js') }}"></script>
@stop