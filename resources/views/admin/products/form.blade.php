<div class="form-group col-md-4">
    <label>Nome</label>
    <input type="text" class="form-control{{$errors->has('name') ? ' is-invalid ': ''}}" value="{{old('name') ?? $register->name ?? ''}}" name="name" id="name" placeholder="Digite o nome">
    @validation(['errors' => $errors->has('name'), 'errors_message' => $errors->first('name')])
    @endvalidation
</div>
<div class="form-group col-md-4">
    <label>Referência</label>
    <input type="text" class="form-control{{$errors->has('reference') ? ' is-invalid ': ''}}" value="{{old('reference') ?? $register->reference ?? ''}}" name="reference" id="reference" placeholder="Digite a referência">
    @validation(['errors' => $errors->has('reference'), 'errors_message' => $errors->first('reference')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>Garantia</label>
    <select class="form-control" name="warranty_number" id="warranty_number">
        @for($i=1;$i<=12;$i++)
            <option value="{{$i}}" {{isset($register) && $register->warranty_number == $i ? "selected" : null}}>{{$i}}</option>
        @endfor
    </select>
</div>
<div class="form-group col-md-2">
    <label>&nbsp;</label>
    <select class="form-control" name="warranty_string" id="warranty_string">
        <option value="Mês(es)" {{isset($register) && $register->warranty_string == "Mês(es)" ? "selected" : null}}>Mês(es)</option>
        <option value="Ano(s)" {{isset($register) && $register->warranty_string == "Ano(s)" ? "selected" : null}}>Ano(s)</option>
    </select>
    <p>&nbsp;</p>
</div>
<div class="form-group col-md-2">
    <label>Tensão</label>
    <select class="form-control" name="tension" id="tension">
        <option value="110 volts" {{isset($register) && $register->tension == "110 volts" ? "selected" : null}}>110 volts</option>
        <option value="220 volts" {{isset($register) && $register->tension == "220 volts" ? "selected" : null}}>220 volts</option>
    </select>
</div><div class="form-group col-md-2">
    <label>Fornecedor</label>
    <select class="form-control" name="provider_id" id="provider_id">
        @foreach($providers as $item)
            <option value="{{$item->id}}" {{isset($register) && $register->provider_id == $item->id ? "selected" : null}}>{{$item->name}}</option>
        @endforeach
    </select>
</div>
<div class="form-group col-md-8">
    <label>Descrição</label>
    <textarea class="form-control{{$errors->has('description') ? ' is-invalid ': ''}}" id="description" name="description" rows="3">{{$register->description ?? ''}}</textarea>
    @validation(['errors' => $errors->has('description'), 'errors_message' => $errors->first('description')])
    @endvalidation
</div>
