<div class="form-group col-md-4">
    <label>Nome</label>
    <input type="text" class="form-control{{$errors->has('name') ? ' is-invalid ' : ''}}" value="{{old('name') ?? ($register->name ?? '')}}" name="name" id="name" placeholder="Digite o nome" >
    @validation(['errors' => $errors->has('name'), 'errors_message' => $errors->first('name')])
    @endvalidation
</div>
<div class="form-group col-md-2">
    <label>Categoria</label>
    <select class="form-control" name="category" id="category" >
        @foreach($categorys as $value)
            <option value="{{$value->id}}" {{(isset($register) && $register->id == $value->id) ? "selected" : ''}}>{{$value->name}}</option>
        @endforeach
    </select>
</div>
<div class="form-group col-md-6">
    <label>Descrição</label>
    <textarea class="form-control{{$errors->has('description') ? ' is-invalid ' : ''}}" id="description" name="description" rows="3">{{old('description') ?? ($register->description ?? '')}}</textarea>
    @validation(['errors' => $errors->has('description'), 'errors_message' => $errors->first('description')])
    @endvalidation
</div>
