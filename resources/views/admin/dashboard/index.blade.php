@extends('adminlte::page')

@section('title', 'Telecontrol')

@section('content_header')
    <div class="headerDashboard">
        <h1>Dashboard</h1>
    </div>
@stop

@section('content')
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fas fa-users"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Clientes</span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-blue-active"><i class="fas fa-industry"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Fornecedores</span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua-active"><i class="fas fa-user-tie "></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Colaboradores</span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red-gradient"><i class="fas fa-box "></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Produtos</span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fas fa-wrench"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Serviços em Aberto</span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fas fa-wrench"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Serviços Finalizados</span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fas fa-wrench"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Total Geral</span>
            </div>
        </div>
    </div>
@stop