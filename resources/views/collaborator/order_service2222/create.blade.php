@extends('adminlte::page')

@section('content_header')
    <div class="headerDashboard">
        <h1>Nova Ordem de Serviço</h1>
    </div>
@stop

@section('css')
    <style>
        .content-wrapper{
            height: 2000px !important;
        }
    </style>
@stop

@section('js')
    <script>

        var table = '';

        $('#amount').val($('#value_service').val());

        function clearInputs(table){

            if(table === 'client'){
                $('#email, #phone , #cep, #street, #number, #neighborhood, #city, #uf').attr("readonly", false).val('');
                $('#client_id').val('0');
            }

            if(table === 'products'){
                $('#reference , #tension, #description_prod').attr("readonly", false).val('');
                $(' #warranty').val('');
                $(' #date_buy').attr("readonly", true).val('');
                $('#product_id').val('0');
            }
        }

        function search(table){

            if(table === 'client'){
                $('#email, #phone , #cep, #street, #number, #neighborhood, #city, #uf').val('buscando...');
            }

            if(table === 'products'){
                $('#reference, #warranty , #tension, #description_prod').val('buscando...');
            }

        }

        function formatMoney(i) {
            var v = i.replace(/\D/g,'');
            v = (v/100).toFixed(2) + '';
            v = v.replace(",", ".");
            v = v.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
            v = v.replace(/(\d)(\d{3}),/g, "$1.$2,");

            return v;
        }

        $('#value_more').on('keyup', function() {

            var value_service = formatMoney($('#value_service').val());
            var value_more = formatMoney($('#value_more').val());

            var amount = parseFloat(value_more) + parseFloat(value_service);

            $('#amount').val(amount);

        });

        $( "#name" ).keyup(function() {

            search('client');

            $.ajax({
                url: "{{ route('client-json') }}",
                type: "post",
                data: {name: $(this).val(), "_token": "{{ csrf_token() }}"},
                dataType: 'json',
                success: function (data) {
                    if(data){
                        $('#email').val(data.email).attr("readonly", true);
                        $('#phone').val(data.phone).attr("readonly", true);
                        $('#cep').val(data.cep);
                        $('#street').val(data.street).attr("readonly", true);
                        $('#number').val(data.number).attr("readonly", true);
                        $('#neighborhood').val(data.neighborhood).attr("readonly", true);
                        $('#city').val(data.city).attr("readonly", true);
                        $('#uf').val(data.uf).attr("readonly", true);
                        $('#client_id').val(data.id);
                    }else{
                        clearInputs('client');
                    }
                }
            });
        });

        $( "#name_prod" ).keyup(function() {

            search('products');

            $.ajax({
                url: "{{ route('product-json') }}",
                type: "post",
                data: {name_prod: $(this).val(), "_token": "{{ csrf_token() }}"},
                dataType: 'json',
                success: function (data) {
                    if(data){
                        $('#reference').val(data.reference).attr("readonly", true);
                        $('#warranty').val(data.warranty).attr("readonly", true);
                        $('#tension').val(data.tension).attr("readonly", true);
                        $('#description_prod').val(data.description).attr("readonly", true);
                        $('#product_id').val(data.id);
                    }else{
                        clearInputs('products')
                    }
                }
            });
        });
    </script>
@stop

@section('content')

    @page_component(['col' => 12])

    @alert(['msg' => session('msg'), 'status' => session('status')])
    @endalert

    @button_back(['routeName' => $routeName])
    @endbutton_back

    @form_component(['action' => route($routeName.".store"), 'method' => "POST"])
    @include('collaborator.order_service.form')
    <div class="form-group col-md-12">
        <button type="submit" class="btn btn-success btn-circle btn-xl marginTop" title="Cadastrar" style="float: right"><i class="fa fa-save"></i></button>
    </div>
    @endform_component

    @endpage_component

@stop




