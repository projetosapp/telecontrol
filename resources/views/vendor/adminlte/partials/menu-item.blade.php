@if (is_string($item))
    @can('ADMINISTRAÇÃO')
        <li class="header">ADMINISTRAÇÃO</li>
    @endcan
    @can('COLABORADOR')
        <li class="header">COLABORADOR</li>
    @endcan
    @can('FORNECEDOR')
        <li class="header">FORNECEDOR</li>
    @endcan
@elseif (isset($item['header']))
    <li class="header">{{ $item['header'] }}</li>
@else
    @can($item['permissions'])
        <li class="{{ $item['class'] }}">
            <a href="{{ $item['href'] }}"
               @if (isset($item['target'])) target="{{ $item['target'] }}" @endif
            >
                <i class="{{ $item['icon'] ?? 'far fa-fw fa-circle' }} {{ isset($item['icon_color']) ? 'text-' . $item['icon_color'] : '' }}"></i>
                <span>
                {{ $item['text'] }}
            </span>

                @if (isset($item['label']))
                    <span class="pull-right-container">
                    <span class="label label-{{ $item['label_color'] ?? 'primary' }} pull-right">{{ $item['label'] }}</span>
                </span>
                @elseif (isset($item['submenu']))
                    <span class="pull-right-container">
                    <i class="fas fa-angle-left pull-right"></i>
                </span>
                @endif
            </a>
            @if (isset($item['submenu']))
                <ul class="{{ $item['submenu_class'] }}">
                    @each('adminlte::partials.menu-item', $item['submenu'], 'item')
                </ul>
            @endif
        </li>
    @endcan
@endif
