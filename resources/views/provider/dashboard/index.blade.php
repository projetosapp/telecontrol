@extends('adminlte::page')

@section('content_header')
    <div class="headerDashboard">
        <h1>Ordem de Serviço</h1>
    </div>
@stop

@section('content')
    @page_component(['col' => 12])

        <div class=" headerTable">
            <div class="col-md-12">
                <form class="form-inline" method="GET" action="{{ route($routeName.'.index') }}">
                    <div class="form-group mx-sm-3 mb-2">
                        <input type="text" class="form-control" id="search" name="search" value="{{$search}}" placeholder="{{$placheholder_search}}">
                    </div>
                    <button type="submit" title="Pesquisar" class="btn btn-circle btn-primary mb-2" style="height: 34px"><i class="fa fa-search"></i></button>
                    <button type="button" title="Limpar Pesquisa" class="btn btn-circle btn-info mb-2" style="height: 34px" id="clear"><i class="fa fa-broom"></i></button>
                </form>
            </div>
        </div><br/>

        @table(['columnList' => $columnList, 'list' => $list, 'routeName' => $routeName])
        @endtable

        @paginate(['search' => $search, 'list' => $list])
        @endpaginate

        @alert(['msg' => session('msg'), 'status' => session('status')])
        @endalert

    @endpage_component
@stop

@section('js')
    <script>
        $('#clear').click(function(){
            $('#search').val("");
            window.location.href = "{{ route($routeName.'.index')}}";
        });
    </script>
@stop