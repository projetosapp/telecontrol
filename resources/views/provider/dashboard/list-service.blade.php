
<br/>
<div class="row col-md-12">
    <div class="col-md-2 text-truncate">
        <b>Código</b>
        <p>{{$order_service->id}}</p>
    </div>
    <div class="col-md-2">
        <b>Inicializado Em</b>
        <p>{{$order_service->service_start}}</p>
    </div>
    <div class="col-md-2">
        <b>Finalizado Em</b>
        <p>{{$order_service->service_finish}}</p>
    </div>
</div>
<div class="col-md-12">
    <hr>
    <h1 style="text-align: center; text-transform: uppercase; font-style: italic; font-size: 15px">Dados do cliente</h1>
    <hr>
</div>
<div class="row col-md-12">
    <div class="col-md-2">
        <b>Nome</b>
        <p>{{$order_service->client->name}}</p>
    </div>
    <div class="col-md-3">
        <b>E-mail</b>
        <p>{{$order_service->client->email}}</p>
    </div>
    <div class="col-md-2">
        <b>Telefone</b>
        <p>{{$order_service->client->phone}}</p>
    </div>
    <div class="col-md-2">
        <b>CEP</b>
        <p>{{$order_service->client->cep}}</p>
    </div>
    <div class="col-md-3">
        <b>Rua</b>
        <p>{{$order_service->client->street}}</p>
    </div>
</div>
<div class="row col-md-12">
    <div class="col-md-2">
        <b>Número</b>
        <p>{{$order_service->client->number}}</p>
    </div>
    <div class="col-md-3">
        <b>Bairro</b>
        <p>{{$order_service->client->neighborhood}}</p>
    </div>
    <div class="col-md-3">
        <b>Cidade</b>
        <p>{{$order_service->client->city.','.$order_service->client->uf}}</p>
    </div>
</div>
<div class="col-md-12">
    <hr>
    <h1 style="text-align: center; text-transform: uppercase; font-style: italic; font-size: 15px">Dados do Produto</h1>
    <hr>
</div>
<div class="row col-md-12">
    <div class="col-md-2">
        <b>Nome</b>
        <p>{{$order_service->product->name}}</p>
    </div>
    <div class="col-md-3">
        <b>Referência</b>
        <p>{{$order_service->product->reference}}</p>
    </div>
    <div class="col-md-2">
        <b>Garantia</b>
        <p>{{$order_service->product->warranty_number.' '.$order_service->product->warranty_string}}</p>
    </div>
    <div class="col-md-2">
        <b>Tensão</b>
        <p>{{$order_service->product->tension}}</p>
    </div>
    <div class="col-md-3">
        <b>Descrição</b>
        <p>{{$order_service->product->description}}</p>
    </div>
</div>
<div class="col-md-12">
    <hr>
    <h1 style="text-align: center; text-transform: uppercase; font-style: italic; font-size: 15px">Funcionario Responsável</h1>
    <hr>
</div>
<div class="row col-md-12">
    <div class="col-md-3">
        <b>Nome</b>
        <p>{{$order_service->collaborator->user->name}}</p>
    </div>
    <div class="col-md-3">
        <b>Área de Atuação</b>
        <p>{{$order_service->service->name}}</p>
    </div>
</div>
<div class="col-md-12">
    <hr>
    <h1 style="text-align: center; text-transform: uppercase; font-style: italic; font-size: 15px">Serviço Prestado</h1>
    <hr>
</div>
<div class="row col-md-12">
    <div class="col-md-3">
        <b>Problema</b>
        <p>{{$order_service->problem}}</p>
    </div>
    <div class="col-md-2">
        <b>Ferramenta Últilizada</b>
        <p>{{$order_service->item->name}}</p>
    </div>
    <div class="col-md-1">
        <b>Nota Fiscal</b>
        <p>{{$order_service->nfe}}</p>
    </div>
    <div class="col-md-3">
        <b>Serviço prestado</b>
        <p>{{$order_service->service->name}}</p>
    </div>
    <div class="col-md-1">
        <b>Valor</b>
        <p>{{$order_service->value_service}}</p>
    </div>
    <div class="col-md-1">
        <b>Valor Adicional</b>
        <p>{{$order_service->value_more}}</p>
    </div>
    <div class="col-md-1">
        <b>Total</b>
        <p>{{$order_service->amount}}</p>
    </div>
</div>