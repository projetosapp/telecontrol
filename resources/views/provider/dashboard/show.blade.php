@extends('adminlte::page')

@section('content_header')
    <div class="headerDashboard">
        <h1>Ordem de Serviço N° {{$order_service->id}}</h1>
    </div>
@stop

@section('css')
    <style>
        .content-wrapper{
            height: 1100px;
        }
    </style>
@stop

@section('content')
    @page_component(['col' => 12])


    @include('provider.dashboard.list-service')

    @endpage_component
@stop
