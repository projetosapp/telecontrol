@php
    if($routeName != 'reports'){
        $routeShow = $routeName.'.show';
    }else{
        $routeShow = $routeNameReports;
    }
@endphp

<table class="table tableList">
    <thead>
    <tr>
        @foreach($columnList as $value)
            <th class="alignCenter">{{$value}}</th>
        @endforeach
            @if($routeName != 'order_service' && $routeName != 'reports' && $routeName != 'dashboard')
                <th class="alignCenter">{{$routeName != 'products' && $routeName != 'clients' && $routeName != 'order_service' ? "Ações" : "Editar"}}</th>
            @endif
    </tr>
    </thead>
    <tbody>
    @foreach($list as $key => $value)
        <tr>
            @foreach($columnList as $key2 => $value2)
                @if($value->{$key2})
                    <td class="alignCenter">
                        @php
                            echo $value->{$key2};
                        @endphp
                    </td>
                @endif
            @endforeach
                <td class="alignCenter">
                    @if($routeName != 'order_service' && $routeName != 'reports' && $routeName != 'dashboard')
                        <a href="{{route($routeName.'.edit',$value->id)}}"><button type="button" id="edit" class="btn btn-primary btnActions"><i class="fas fa-edit"></i></button></a>
                    @else
                        <a href="{{route($routeShow,$value->id)}}"><button type="button" title="Visualizar" class="btn btn-warning btnActions"><i class="fas fa-eye"></i></button></a>
                    @endif
                    @if($routeName != 'products' && $routeName != 'clients' && $routeName != 'order_service' && $routeName != 'reports' && $routeName != 'dashboard')
                        <a href="{{route($inactiveRoute, $value->id)}}"><button type="button" id="block" class="btn btn-{{$value->active == "Sim" ? 'danger' : 'info'}} btnActions"><i class="fas fa-{{$value->active == "Sim" ? 'ban' : 'check'}}"></i></button></a>
                    @endif
                </td>
        </tr>
    @endforeach
    </tbody>
</table>