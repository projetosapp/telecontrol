@if($errors)
    <small class="form-text text-muted">
        <strong>{{$errors_message}}</strong>
    </small>
@else
    <small class="form-text text-muted">
        <strong>&nbsp;</strong>
    </small>
@endif