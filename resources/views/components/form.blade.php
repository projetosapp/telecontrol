@php
    $method = strtolower($method);
    $method_input = "";

    if($method == "post"){
        $method = "post";
    }elseif($method == "put"){
        $method = "post";
        $method_input = method_field('PUT');
    }elseif($method == 'delete'){
        $method = "delete";
        $method_input = method_field('DELETE');
    }else{
        $method = 'get';
    }
@endphp

<form autocomplete="off" action="{{$action}}" method="{{$method}}">
    @csrf
    {{$method_input}}
    {{$slot}}
</form>


@section('js')

    <script>
        (function( $ ) {
            $(function() {
                $("#phone").mask("(99) 999-9999");
                $("#cep").mask("99.999-999");
                $("#cpf").mask("99.999.999-99");
                $('#diaria').mask('#.##0,00', {reverse: true});
            });
        })(jQuery);

    </script>
@stop