@if ($msg)
    <div class="callout callout-{{$status}} lead">
        <p>{{$msg}}</p>
    </div>
@endif