@php

if($routeName == 'products'){
    $colButton = '1';
    $colSearch = '9';
}else{
    $colButton = '4';
    $colSearch = '8';
}

if(!isset($placheholder_search)){
    $placheholder_search = 'Pesquisar';
}

@endphp

<div class=" headerTable">
    <div class="col-md-{{$colSearch}}">
        <form class="form-inline" method="GET" action="{{ route($routeName.'.index') }}">
            <div class="form-group mx-sm-3 mb-2">
                <input type="text" class="form-control" id="search" name="search" value="{{$search}}" placeholder="{{$placheholder_search}}">
            </div>
            <button type="submit" title="Pesquisar" class="btn btn-circle btn-primary mb-2" style="height: 34px"><i class="fa fa-search"></i></button>
            <button type="button" title="Limpar Pesquisa" class="btn btn-circle btn-info mb-2" style="height: 34px" id="clear"><i class="fa fa-broom"></i></button>
        </form>
    </div>
    <div class="col-md-3">
        <a href="{{ route($routeName.'.create')  }}">
            <button class="btn btn-info btn-circle btn-xl" title="Cadastrar"><i class="fa fa-plus"></i></button>
        </a>
    </div>
</div>

@section('js')
    <script>
        $('#clear').click(function(){
            $('#search').val("");
            window.location.href = "{{ route($routeName.'.index')}}";
        });
    </script>
@stop