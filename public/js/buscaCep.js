$(document).ready(function() {

    $("#phone").mask("(99) 999-9999");
    $("#cep").mask("99.999-999");
    $("#cpf").mask("99.999.999-99");
    $('#diaria, #value_more, #value_service').mask('#.##0,00', {reverse: true});

    function clear_form() {
        //Limpa os valores do formulário

        $("#street").val("");
        $("#neighborhood").val("");
        $("#city").val("");
        $("#uf").val("");
        $("#cep").val("");
    }

    $("#cep").blur(function() {

        var cep = $(this).val().replace(/\D/g, '');

        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validatecep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validatecep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#street").val("...");
                $("#neighborhood").val("...");
                $("#city").val("...");
                $("#uf").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#street").val(dados.logradouro);
                        $("#neighborhood").val(dados.bairro);
                        $("#city").val(dados.localidade);
                        $("#uf").val(dados.uf);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        clear_form();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                clear_form();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            clear_form();
        }
    });
});
