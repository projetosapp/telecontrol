<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Services extends Authenticatable
{
    protected $fillable = [
        'name', 'description', 'value', 'warranty_number', 'warranty_string', 'active'
    ];

}
