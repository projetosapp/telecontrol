<?php

namespace App\Repositories\Contracts;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

interface ClientsRepositoryInterface
{
    public function all(string $column = 'id', string $order = 'ASC'): Collection;
    public function paginate(int $paginate = 10, string $column = 'id', string $order = 'ASC'): LengthAwarePaginator;
    public function findWhereLike(array $columns, string $search, string $column = 'id', string $order = 'ASC'): Collection;
    public function create(array $data): Bool;
    public function clientsFunction(int $id);
    public function findWhereLikeClients(array $columns, string $search, string $column = 'id', string $order = 'ASC');
    public function find(int $id);
    public function update(array $data, int $id);
    public function block($data, int $id);
    public function createClient(array $data);

}