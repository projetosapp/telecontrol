<?php

namespace App\Repositories\Contracts;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

interface OrderServiceRepositoryInterface
{
    public function all(string $column = 'id', string $order = 'ASC'): Collection;
    public function paginate(int $paginate = 10, string $column = 'id', string $order = 'ASC'): LengthAwarePaginator;
    public function findWhereLike(array $columns, string $search, string $column = 'id', string $order = 'ASC'): Collection;
    public function create(array $data): Bool;
    public function find(int $id);
    public function update(array $data, int $id);
    public function findClientName(string $name);
    public function findProductName(string $name);
    public function findCollaborator(string $user_id);
    public function findWhereOrder(array $columns, string $search, string $column = 'id', string $order = 'ASC', $type);
    public function paginateOrder(int $paginate = 10, string $column = 'id', string $order = 'ASC', $type);
    public function countOrders(int $type);
}