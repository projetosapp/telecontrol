<?php

namespace App\Repositories\Eloquent;

use App\User;
use App\Repositories\Contracts\UserRepositorieInterface;
use Illuminate\Support\Facades\DB;

class UserRepository extends AbstractRepository implements UserRepositorieInterface
{

    protected $model = User::class;

    public function userAll(int $id)
    {
        $users = DB::table('users')->where('id', $id)->first();

        return $users;
    }

}