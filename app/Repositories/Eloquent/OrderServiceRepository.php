<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\OrderServiceRepositoryInterface;
use App\OrderService;
use Illuminate\Support\Facades\DB;

class OrderServiceRepository extends AbstractRepository implements OrderServiceRepositoryInterface
{

    protected $model = OrderService::class;

    public function findCollaborator(string $user_id){

        $coll = DB::table('users')
            ->join('collaborators', 'users.id', '=', 'collaborators.user_id')
            ->where('users.id', $user_id)
            ->first();

        return $coll;
    }

    public function findWhereOrder(array $columns, string $search, string $column = 'id', string $order = 'ASC', $type)
    {
        $query = $this->model;

        $date_now = new \DateTime();

        foreach ($columns as $key => $value){

            if($type == 1){
                $query = $query->Where($value, 'like', '%'.$search.'%')->Where('service_finish', ">=", $date_now);
            }elseif($type == 2){
                $query = $query->Where($value, 'like', '%'.$search.'%')->Where('service_finish', "<=", $date_now);
            }else{
                $query = $query->Where($value, 'like', '%'.$search.'%');
            }
        }

        return $query->orderBy($column, $order)->get();
    }

    public function countOrders(int $type){

        $query = $this->model;

        $date_now = new \DateTime();

        if($type == 1){
            $query = $query->Where('service_finish', ">=", $date_now);
        }elseif($type == 2){
            $query = $query->Where('service_finish', "<=", $date_now);
        }

        return $query;
    }

    public function paginateOrder(int $paginate = 10, string $column = 'id', string $order = 'ASC', $type)
    {
        $date_now = new \DateTime();

        if($type == 1){
            $query = $this->model->Where('service_finish', '>=', $date_now)->orderBy($column, $order)->paginate($paginate);
        }elseif($type == 2){
            $query = $this->model->Where('service_finish', '<=', $date_now)->orderBy($column, $order)->paginate($paginate);
        }else{
            $query = $this->model->orderBy($column, $order)->paginate($paginate);
        }

        return $query;
    }

}