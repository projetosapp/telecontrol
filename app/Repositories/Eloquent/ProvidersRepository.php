<?php

namespace App\Repositories\Eloquent;

use App\Providers;
use App\Repositories\Contracts\ProvidersRepositoryInterface;
use Illuminate\Support\Facades\DB;

class ProvidersRepository extends AbstractRepository implements ProvidersRepositoryInterface
{

    protected $model = Providers::class;

    public function findWhereLikeProviders(array $columns, string $search, string $column = 'id', string $order = 'ASC'){

        foreach ($columns as $key => $value) {
            $providers = DB::table('users')
                ->join('providers', 'users.id', '=', 'providers.user_id')
                ->orWhere($value, 'like', '%' . $search . '%')
                ->get();
        }


        return $providers;

    }

}