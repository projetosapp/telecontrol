<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\CollaboratorsRepositoryInterface;
use App\Collaborators;
use Illuminate\Support\Facades\DB;

class CollaboratorsRepository extends AbstractRepository implements CollaboratorsRepositoryInterface
{

    protected $model = Collaborators::class;

    public function collaboratorFunction(int $id)
    {
        $service_id = DB::table('collaborators')->where('id', $id)->value('service_id');
        $service_name = DB::table('services')->where('id', $service_id)->value('name');

        return $service_name;
    }

    public function findWhereLikeCollaborator(array $columns, string $search, string $column = 'id', string $order = 'ASC'){

        foreach ($columns as $key => $value) {
            $colaborators = DB::table('users')
                ->join('collaborators', 'users.id', '=', 'collaborators.user_id')
                ->orWhere($value, 'like', '%' . $search . '%')
                ->get();
        }


        return $colaborators;

    }


}