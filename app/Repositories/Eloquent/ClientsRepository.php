<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\ClientsRepositoryInterface;
use App\Clients;
use Illuminate\Support\Facades\DB;

class ClientsRepository extends AbstractRepository implements ClientsRepositoryInterface
{

    protected $model = Clients::class;
    protected $model_user = User::class;

    public function clientsFunction(int $id)
    {
        $service_id = DB::table('itens')->where('id', $id)->value('service_id');
        $service_name = DB::table('services')->where('id', $service_id)->value('name');

        return $service_name;
    }

    public function findWhereLikeClients(array $columns, string $search, string $column = 'id', string $order = 'ASC'){

        foreach ($columns as $key => $value) {
            $clients = DB::table('users')
                ->join('clients', 'users.id', '=', 'clients.user_id')
                ->orWhere($value, 'like', '%' . $search . '%')
                ->get();
        }

        return $clients;

    }

}