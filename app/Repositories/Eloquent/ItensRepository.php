<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\ItensRepositoryInterface;
use App\Itens;
use Illuminate\Support\Facades\DB;

class ItensRepository extends AbstractRepository implements ItensRepositoryInterface
{

    protected $model = Itens::class;


}