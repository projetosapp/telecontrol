<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\ServicesRepositoryInterface;
use App\Services;

class ServicesRepository extends AbstractRepository implements ServicesRepositoryInterface
{

    protected $model = Services::class;

}