<?php

namespace App\Repositories\Eloquent;

use App\Products;
use App\Repositories\Contracts\ProductsRepositoryInterface;

class ProductsRepository extends AbstractRepository implements ProductsRepositoryInterface
{

    protected $model = Products::class;

}