<?php

namespace App\Repositories\Eloquent;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

abstract class AbstractRepository
{
    protected $model;

    function __construct()
    {
        $this->model = $this->resolveModel();
    }

    public function all(string $column = 'id', string $order = 'ASC'):Collection
    {
        return $this->model->orderBy($column, $order)->get();
    }

    public function create(array $data):Bool
    {
        return (bool) $this->model->create($data);
    }

    public function createProduct(array $data)
    {
        return $this->model->create($data);
    }

    public function createClient(array $data)
    {
        return $this->model->create($data);
    }

    public function createUser(array $data)
    {
        return $this->model->create($data);
    }

    public function paginate(int $paginate = 10, string $column = 'id', string $order = 'ASC'):LengthAwarePaginator
    {
        return $this->model->orderBy($column, $order)->paginate($paginate);
    }

    public function findWhereLike(array $columns, string $search, string $column = 'id', string $order = 'ASC'):Collection
    {
        $query = $this->model;

        foreach ($columns as $key => $value){
            $query = $query->orWhere($value, 'like', '%'.$search.'%');
        }

        return $query->orderBy($column, $order)->get();
    }

    public function find(int $id)
    {
        $register = $this->model->find($id);

        if($register){
            return $register;
        }

        return false;
    }

    public function update(array $data, int $id)
    {
        $register = $this->find($id);

        if($register->user){
            $register->user->update($data);
        }

        if($register){
            return (bool) $register->update($data);
        }

        return false;
    }

    public function block($data, int $id)
    {

        $register = $this->find($id);

        if($register){
            return (bool) $register->update($data);
        }

        return false;
    }

    public function findClientName(string $name){

        $client = DB::table('clients')
            ->orWhere('name', 'like', '%' . $name . '%')
            ->get();

        return $client;
    }

    public function findProductName(string $name){

        $product = DB::table('products')
            ->orWhere('name', 'like', '%' . $name . '%')
            ->get();

        return $product;
    }

    protected function resolveModel()
    {
        return app($this->model);
    }
}