<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{

    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];


    public function boot()
    {
        $this->registerPolicies();
        $user = Auth::user();
        /*
         * Permissions header LINKS
         */

        Gate::define('ADMINISTRAÇÃO', function ($user){

            if($user->type == 1){
                return true;
            }

            return false;

        });

        Gate::define('COLABORADOR', function ($user){

            if($user->type == 2){
                return true;
            }

            return false;

        });

        Gate::define('FORNECEDOR', function ($user){

            if($user->type == 3){
                return true;
            }

            return false;

        });

        Gate::define('links-admin', function ($user){
            if($user->type == 1){
                return true;
            }

            return false;
        });

        Gate::define('links-collaborator', function ($user){
            if($user->type == 2){
                return true;
            }

            return false;
        });

        Gate::define('links-provider', function ($user){
            if($user->type == 3){
                return true;
            }

            return false;
        });

        Gate::define('URL_ADMIN', function ($user){
            if($user->type == 1){
                return true;
            }

            return false;
        });

        Gate::define('URL_COLABORADOR', function ($user){
            if($user->type == 2){
                return true;
            }

            return false;
        });

        Gate::define('URL_FORNECEDOR', function ($user){
            if($user->type == 3){
                return true;
            }

            return false;
        });

        //
    }
}
