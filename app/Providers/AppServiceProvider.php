<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(
            'App\Repositories\Contracts\UserRepositorieInterface',
            'App\Repositories\Eloquent\UserRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\ServicesRepositoryInterface',
            'App\Repositories\Eloquent\ServicesRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\CollaboratorsRepositoryInterface',
            'App\Repositories\Eloquent\CollaboratorsRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\ItensRepositoryInterface',
            'App\Repositories\Eloquent\ItensRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\ClientsRepositoryInterface',
            'App\Repositories\Eloquent\ClientsRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\ProductsRepositoryInterface',
            'App\Repositories\Eloquent\ProductsRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\ProvidersRepositoryInterface',
            'App\Repositories\Eloquent\ProvidersRepository'
        );

        $this->app->bind(
            'App\Repositories\Contracts\OrderServiceRepositoryInterface',
            'App\Repositories\Eloquent\OrderServiceRepository'
        );
    }


    public function boot()
    {
        Blade::component('components.alert', 'alert');
        Blade::component('components.header_page', 'header_page');
        Blade::component('components.table', 'table');
        Blade::component('components.paginate', 'paginate');
        Blade::component('components.page', 'page_component');
        Blade::component('components.form', 'form_component');
        Blade::component('components.button_back', 'button_back');
        Blade::component('components.validation', 'validation');
        Blade::component('components.modal_import', 'modal_import');
    }
}
