<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Products extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'reference', 'warranty_number', 'warranty_string', 'description', 'tension', 'provider_id'
    ];

    public function provider()
    {
        return $this->belongsTo('App\Providers');
    }

}
