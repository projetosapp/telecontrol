<?php

namespace App\Imports;

use App\Products;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class ProductsImport implements ToModel
{
    public function model(array $row)
    {
        return new Products([
            'name'     => $row[1],
            'reference'    => $row[2],
            'warranty_number' => $row[3],
            'warranty_string' => $row[4],
            'tension' => $row[5],
            'description' => $row[6],
        ]);
    }
}
