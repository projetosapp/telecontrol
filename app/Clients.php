<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Clients extends Authenticatable
{
    protected $fillable = [
        'phone',
        'name',
        'email',
        'following',
        'cep',
        'neighborhood',
        'street',
        'number',
        'city',
        'uf',
    ];

}
