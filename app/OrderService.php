<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class OrderService extends Authenticatable
{

    protected $fillable = [

        'product_id',
        'service_id',
        'collaborator_id',
        'client_id',
        'item_id',
        'nfe',
        'product_problem',
        'value_service',
        'value_more',
        'description',
        'problem',
        'service_start',
        'service_finish',
        'amount'

    ];

    public function service()
    {
        return $this->belongsTo('App\Services');
    }

    public function client()
    {
        return $this->belongsTo('App\Clients');
    }

    public function product()
    {
        return $this->belongsTo('App\Products');
    }

    public function collaborator()
    {
        return $this->belongsTo('App\Collaborators');
    }

    public function item()
    {
        return $this->belongsTo('App\Itens');
    }

}
