<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function redirectHome(){
        $user = Auth::user();

        if($user->type == 1){
            return redirect('/admin/dashboard');
        }elseif($user->type == 2){
            return redirect('/collaborator/dashboard');
        }elseif($user->type == 3){
            return redirect('/provider/dashboard');
        }
    }
}
