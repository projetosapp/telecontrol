<?php

namespace App\Http\Controllers\Collaborator;

use App\Clients;
use App\Itens;
use App\Products;
use App\Repositories\Contracts\ClientsRepositoryInterface;
use App\Repositories\Contracts\OrderServiceRepositoryInterface;
use App\Repositories\Contracts\ProductsRepositoryInterface;
use App\Repositories\Contracts\ServicesRepositoryInterface;
use App\Services;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrderServiceController extends Controller
{
    protected $route = 'order_service';
    protected $paginate = 5;
    protected $model = '';
    protected $model_product = '';
    protected $model_client = '';

    public function __construct(
        OrderServiceRepositoryInterface $model,
        ServicesRepositoryInterface $model_service,
        ProductsRepositoryInterface $model_product,
        ClientsRepositoryInterface $model_client
    )
    {
        $this->middleware('admin');
        $this->model = $model;
        $this->model_service = $model_service;
        $this->model_product = $model_product;
        $this->model_client = $model_client;
    }

    public function index(Request $request)
    {
        $columnList = [
            'id' => 'Código',
            'name_client' => 'Cliente',
            'service_prested' => 'Serviço prestado',
            'product' => 'Produto',
            'value_service' => 'Valor',
            'service_start' => 'Iniciado Em',
            'service_finish' => 'Finalizado Em',
            'view' => 'Visualizar'
        ];

        $search = '';

        if(isset($request->search)){
            $search = $request->search;
            $list = $this->model->findWhereLike([
                'id',
            ], $search, 'id', 'DESC');

        }else{

            $list = $this->model->paginate($this->paginate, 'id', 'DESC');
        }

        foreach ($list as $key => $item){

            $date_start = strtotime($item->service_start);
            $date_finish = strtotime($item->service_finish);

            $list[$key]['name_client'] = $item->client->name;
            $list[$key]['service_prested'] = $item->service->name;
            $list[$key]['product'] = $item->product->name;
            $list[$key]['service_start'] = date('d/m/Y', $date_start);
            $list[$key]['service_finish'] = date('d/m/Y', $date_finish);
            $list[$key]['value_service'] = "R$ ".number_format($item['value_service'],2,",",".");

        }

        $routeName = $this->route;
        $placheholder_search = 'Digite o código';

        return view('collaborator.'.$routeName.'.index', compact('list', 'search', 'routeName', 'columnList', 'placheholder_search'));

    }

    public function create(){

        $routeName = $this->route;
        $itens = Itens::all();
        $userOn = Auth::user();
        $coll_data = $this->model->findCollaborator($userOn->id);
        $service_coll = $this->model_service->find($coll_data->service_id)->toArray();

        return view('collaborator.'.$routeName.'.create', compact('routeName', 'data_services', 'itens', 'coll_data', 'service_coll'));

    }

    public function show($id){

        $routeName =  $this->model->find($id);

        $order_service = $this->model->find($id);

        $date_start = strtotime($order_service->service_start);
        $date_finish = strtotime($order_service->service_finish);


        $order_service->service_start = date('d/m/Y', $date_start);
        $order_service->service_finish = date('d/m/Y', $date_finish);
        $order_service->value_service = "R$ ".number_format($order_service->value_service,2,",",".");
        $order_service->value_more = "R$ ".number_format($order_service->value_more,2,",",".");
        $order_service->amount = "R$ ".number_format($order_service->amount,2,",",".");

        return view('collaborator.order_service.show', compact('routeName','order_service'));

    }

    public function returnClientJson(Request $request){

        if($request->ajax() && $request->name){
            $dados_client = $this->model->findClientName($request->name);

            if(count($dados_client)){
                $data = [];

                $data['id'] = $dados_client[0]->id;
                $data['name'] = $dados_client[0]->name;
                $data['email'] = $dados_client[0]->email;
                $data['phone'] = $dados_client[0]->phone;
                $data['cep'] = $dados_client[0]->cep;
                $data['number'] = $dados_client[0]->number;
                $data['street'] = $dados_client[0]->street;
                $data['uf'] = $dados_client[0]->uf;
                $data['neighborhood'] = $dados_client[0]->neighborhood;
                $data['city'] = $dados_client[0]->city;

                return $data;
            }
        }

        return 0;
    }

    public function returnProductJson(Request $request){

        if($request->ajax() && $request->name_prod){
            $dados_client = $this->model->findProductName($request->name_prod);

            if(count($dados_client)){

                $data = [];

                $data['id'] = $dados_client[0]->id;
                $data['name_prod'] = $dados_client[0]->name;
                $data['reference'] = $dados_client[0]->reference;
                $data['warranty'] = $dados_client[0]->warranty_number . ' ' . $dados_client[0]->warranty_string;
                $data['tension'] = $dados_client[0]->tension;
                $data['description'] = $dados_client[0]->description;

                return $data;
            }
        }
        return 0;
    }

    public function pdfService($id){

        $service = $this->show($id);
        $service = $service->order_service;

        $date_start = strtotime($service->service_start);
        $date_finish = strtotime($service->service_finish);


        $service->service_start = date('d/m/Y', $date_start);
        $service->service_finish = date('d/m/Y', $date_finish);
        $service->warranty = $service->product->warranty_number.' '.$service->product->warranty_string;
        
        $valor =  \PDF::loadView('collaborator.order_service.pdf-service', compact('service'))
            ->download('ordem-de-servico.pdf');
        return $valor;
    }

    public function store(Request $request){
        $data = $request->all();

        if(!$request['client_id']){

            $data_client = [];

            Validator::make($data, [
                'name' => 'required|string|max:60',
                'email' => ['required', 'string', 'email', 'max:255'],
                'phone' => 'required|string',
                'number' => 'integer',
                'cep' => 'required|string|max:35',
            ])->validate();


            $data_client['name'] = $data['name'];
            $data_client['email'] = $data['email'];
            $data_client['phone'] = $data['phone'];
            $data_client['number'] = $data['number'];
            $data_client['cep'] = $data['cep'];
            $data_client['street'] = $data['street'];
            $data_client['neighborhood'] = $data['neighborhood'];
            $data_client['city'] = $data['city'];
            $data_client['uf'] = $data['uf'];

            $new_client = $this->model_client->createClient($data_client);

            if(!$new_client)
            {
                session()->flash('msg', 'Erro ao adicionar Produto');
                session()->flash('status', 'danger');
                return redirect()->back();
            }
        }

        if(!$request['product_id']){

            $data_product = [];

            Validator::make($data, [
                'name_prod' => 'required|string|max:60',
                'reference' => 'required|string|max:255',
                'description' => 'required|string|max:255',
            ])->validate();

            $data_product['name'] = $data['name_prod'];
            $data_product['reference'] = $data['reference'];
            $data_product['tension'] = $data['tension'];
            $data_product['description'] = $data['description_prod'];

            $new_product = $this->model_product->createProduct($data_product);

            if(!$new_product)
            {
                session()->flash('msg', 'Erro ao adicionar Produto');
                session()->flash('status', 'danger');
                return redirect()->back();
            }
        }

        Validator::make($data, [
            'nfe' => 'required|integer',
            'description' => 'required|string|max:255',
            'problem' => 'string|required|max:255',
            'service_start' => 'required|date|',
            'service_finish' => 'required|date|',

        ])->validate();


        $data['value_service'] = (float) str_replace(',', '.', $data['value_service']);
        $data['value_more'] = (float) str_replace(',', '.', $data['value_more']);
        $data['amount'] = (float) str_replace(',', '.', $data['amount']);
        $data['product_id'] = !$data['product_id'] ? $new_product->id : $data['product_id'];
        $data['client_id'] = !$data['client_id'] ? $new_client->id : $data['client_id'];


        if($this->model->createProduct($data))
        {
            session()->flash('msg', 'Ordem de serviço gerada com sucesso');
            session()->flash('status', 'success');
            return redirect()->route($this->route.'.index');
        }else{
            session()->flash('msg', 'Error ao adicionar ordem de serviço');
            session()->flash('status', 'danger');
            return redirect()->route($this->route.'.index');
        }

    }

}
