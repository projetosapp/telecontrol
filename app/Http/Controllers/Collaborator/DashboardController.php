<?php

namespace App\Http\Controllers\Collaborator;

use App\OrderService;
use App\Repositories\Contracts\OrderServiceRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class DashboardController extends Controller
{
    protected $model = '';

    public function __construct(OrderServiceRepositoryInterface $model)
    {
        $this->middleware('collaborator');
        $this->model = $model;
    }

    public function index()
    {
        $total_geral = DB::table('order_services')->get();
        $open = $this->model->countOrders(1);
        $closed = $this->model->countOrders(2);

        return view('collaborator.dashboard.index', compact('total_geral', 'open', 'closed'));
    }
}
