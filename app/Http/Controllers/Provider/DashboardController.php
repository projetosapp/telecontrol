<?php

namespace App\Http\Controllers\Provider;

use App\Repositories\Contracts\OrderServiceRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    protected $route = 'dashboard';
    protected $paginate = 5;
    protected $model = '';

    public function __construct(OrderServiceRepositoryInterface $model)
    {
        $this->middleware('provider');
        $this->model = $model;
    }

    public function index(Request $request, $type = 1)
    {
        $columnList = [
            'id' => 'Código',
            'name_client' => 'Cliente',
            'service_prested' => 'Serviço prestado',
            'product' => 'Produto',
            'value_service' => 'Valor',
            'service_start' => 'Iniciado Em',
            'service_finish' => 'Finalizado Em',
            'status' => 'Situação',
            'view' => 'Visualizar'
        ];

        $search = '';

        if(isset($request->search)){
            $search = $request->search;
            $list = $this->model->findWhereOrder([
                'id',
            ], $search, 'id', 'DESC', $type);

        }else{

            $list = $this->model->paginateOrder($this->paginate, 'id', 'DESC', $type);

        }

        foreach ($list as $key => $item){

            $date_start = strtotime($item->service_start);
            $date_finish = strtotime($item->service_finish);

            $date_now = new \DateTime();
            $date_now = strtotime($date_now->format('Y-m-d 00:00:00'));

            $list[$key]['name_client'] = $item->client->name;
            $list[$key]['service_prested'] = $item->service->name;
            $list[$key]['product'] = $item->product->name;
            $list[$key]['service_start'] = date('d/m/Y', $date_start);
            $list[$key]['service_finish'] = date('d/m/Y', $date_finish);
            $list[$key]['value_service'] = "R$ ".number_format($item['value_service'],2,",",".");
            $list[$key]['status'] = $date_finish <= $date_now ? "<p style='color:green; font-weight: bold;'>Finalizado</p>" : "<p style='color:orange; font-weight: bold;'>Em Aberto</p>";
        }


        $routeName = $this->route;
        $placheholder_search = 'Digite o código';
        $routeType = 1;

        return view('provider.'.$routeName.'.index', compact('list', 'search', 'routeName', 'columnList', 'placheholder_search' ,'routeType', 'type'));

    }

    public function show($id){

        $routeName =  $this->model->find($id);

        $order_service = $this->model->find($id);

        $date_start = strtotime($order_service->service_start);
        $date_finish = strtotime($order_service->service_finish);


        $order_service->service_start = date('d/m/Y', $date_start);
        $order_service->service_finish = date('d/m/Y', $date_finish);
        $order_service->value_service = "R$ ".number_format($order_service->value_service,2,",",".");
        $order_service->value_more = "R$ ".number_format($order_service->value_more,2,",",".");
        $order_service->amount = "R$ ".number_format($order_service->amount,2,",",".");

        return view('provider.dashboard.show', compact('routeName','order_service'));

    }
}
