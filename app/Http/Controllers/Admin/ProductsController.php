<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Exports\ProductsExport;
use App\Imports\ProductsImport;
use App\Products;
use function foo\func;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use App\Providers;
use App\Repositories\Contracts\ProductsRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{

    protected $route = 'products';
    protected $paginate = 5;
    protected $model = '';

    public function __construct(ProductsRepositoryInterface $model)
    {
        $this->middleware('admin');
        $this->model = $model;
    }


    public function index(Request $request)
    {
        $columnList = [
            'name' => 'Nome',
            'provider' => 'Fornecedor',
            'description' => 'Descrição',
        ];

        $search = '';

        if(isset($request->search)){
            $search = $request->search;
            $list = $this->model->findWhereLike([
                'name',
                'description'
            ], $search, 'id', 'DESC');
        }else{

            $list = $this->model->paginate($this->paginate, 'id', 'DESC');
        }

        foreach ($list as $key => $item){
            $list[$key]->warranty = $item->warranty_number.' '.$item->warranty_string;
            $list[$key]->provider = $item->provider ? $item->provider->name : '--';
            $list[$key]->active = $item->active ? "Sim" : "Não";
            $list[$key]->description = mb_strimwidth($item->description, 0, 80, "...");
        }

        $routeName = $this->route;

        return view('admin.'.$routeName.'.index', compact('list', 'search', 'routeName', 'columnList'));

    }

    public function create()
    {
        $providers = Providers::all();
        $routeName = $this->route;

        return view('admin.'.$routeName.'.create', compact('routeName', 'providers'));
    }

    public function edit($id)
    {

        $routeName = $this->route;
        $register = $this->model->find($id);
        $providers = Providers::all();

        if($register){
            return view('admin.'.$routeName.'.edit', compact('routeName', 'register', 'providers'));
        }

        return redirect()->route($routeName.'.index');

    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        Validator::make($data, [
            'name' => 'required|string|max:60',
            'description' => 'required', 'string', 'max:255',
        ])->validate();

        if($this->model->update($data, $id))
        {
            session()->flash('msg', 'Item alterado com sucesso');
            session()->flash('status', 'success');
            return redirect()->back();
        }else{
            session()->flash('msg', 'Erro ao alterar Item');
            session()->flash('status', 'danger');
            return redirect()->back();
        }

    }

    public function inactive($id){

        $routeName = $this->route;

        $data = $this->model->find($id)->toArray();
        $msg = '';

        $data['active'] = $data['active'] ? 0 : 1;
        $msg = $data['active'] ? "ativado" : "desativado";

        if($this->model->block($data, $id)){
            session()->flash('msg', 'Produto '.$msg.' com sucesso');
            session()->flash('status', 'success');
        }else{
            session()->flash('msg', 'Erro ao '.$msg.' Produto');
            session()->flash('status', 'danger');
        }

        return redirect()->route($routeName.'.index');
    }

    public function store(Request $request){

        $data = $request->all();

        Validator::make($data, [
            'name' => 'required|string|max:60',
            'reference' => 'required|string|max:255',
            'description' => 'required|string|max:255',
        ])->validate();

        if($this->model->create($data))
        {
            session()->flash('msg', 'Produto cadastrado com sucesso');
            session()->flash('status', 'success');
            return redirect()->back();
        }else{
            session()->flash('msg', 'Erro ao adicionar Produto');
            session()->flash('status', 'danger');
            return redirect()->back();
        }

    }

    public function export($name)
    {
        return Excel::download(new ProductsExport, 'produtos.xlsx');
    }

    public function import(Request $request)
    {
        $routeName = $this->route;

        if($request->hasFile('file') && $request->file('file')->isValid()){

            $extension = $request->file->extension();

            if($extension != 'xlsx' && $extension != 'csv'){
                session()->flash('msg', 'É apenas permitido enviar arquivos xlsx ou csv');
                session()->flash('status', 'warning');
                return redirect()->route($routeName.'.index');
            }

            $nameFile = "produtos.".$extension;

            $upload = $request->file->storeAs('produtos', $nameFile);

            if($upload){

                Excel::import(new ProductsImport, storage_path('app/public/produtos/'.$nameFile));

                session()->flash('msg', 'Produtos cadastrados com sucesso');
                session()->flash('status', 'success');

                return redirect()->route($routeName.'.index');
            }

        }

        session()->flash('msg', 'Error ao fazer upload do arquivo');
        session()->flash('status', 'danger');
        return redirect()->route($routeName.'.index');
    }

}
