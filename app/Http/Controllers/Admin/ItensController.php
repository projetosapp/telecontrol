<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Repositories\Contracts\ItensRepositoryInterface;
use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ItensController extends Controller
{

    protected $route = 'itens';
    protected $paginate = 5;
    protected $model = '';
    protected $inactive_route = 'block_item';

    public function __construct(ItensRepositoryInterface $model)
    {
        $this->middleware('admin');
        $this->model = $model;
    }


    public function index(Request $request)
    {
        $columnList = [
            'name' => 'Nome',
            'description' => 'Descrição',
            'service_id' => 'Serviço',
            'active' => 'Ativo',
        ];

        $search = '';

        if(isset($request->search)){
            $search = $request->search;
            $list = $this->model->findWhereLike([
                'name',
                'description'
            ], $search, 'id', 'DESC');
        }else{

            $list = $this->model->paginate($this->paginate, 'id', 'DESC');
        }

        foreach ($list as $key =>  $item){

            $list[$key]->active = $item->active ? "Sim" : "Não";
            $list[$key]->service_id = $item->service->name;
            $list[$key]->description = mb_strimwidth($item->description, 0, 80, "...");

        }

        $routeName = $this->route;
        $inactiveRoute = $this->inactive_route;


        return view('admin.'.$routeName.'.index', compact('list', 'search', 'routeName', 'columnList', 'inactiveRoute'));

    }

    public function create()
    {
        $routeName = $this->route;

        $categorys = Services::all();

        return view('admin.'.$routeName.'.create', compact('routeName', 'categorys'));
    }

    public function edit($id)
    {

        $routeName = $this->route;
        $register = $this->model->find($id);
        $categorys = Services::all();

        if($register){
            return view('admin.'.$routeName.'.edit', compact('routeName', 'categorys', 'register'));
        }

        return redirect()->route($routeName.'.index');

    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        Validator::make($data, [
            'name' => 'required|string|max:60',
            'description' => 'required', 'string', 'max:255',
        ])->validate();

        if($this->model->update($data, $id))
        {
            session()->flash('msg', 'Item alterado com sucesso');
            session()->flash('status', 'success');
            return redirect()->back();
        }else{
            session()->flash('msg', 'Erro ao alterar Item');
            session()->flash('status', 'danger');
            return redirect()->back();
        }

    }

    public function inactive($id){

        $routeName = $this->route;

        $data = $this->model->find($id)->toArray();
        $msg = '';

        $data['active'] = $data['active'] ? 0 : 1;
        $msg = $data['active'] ? "ativado" : "desativado";

        if($this->model->block($data, $id)){
            session()->flash('msg', 'Item '.$msg.' com sucesso');
            session()->flash('status', 'success');
        }else{
            session()->flash('msg', 'Erro ao '.$msg.' Item');
            session()->flash('status', 'danger');
        }

        return redirect()->route($routeName.'.index');
    }

    public function store(Request $request){

        $data = $request->all();

        Validator::make($data, [
            'name' => 'required|string|max:60',
            'description' => 'required', 'string', 'max:255',
        ])->validate();


        $data['service_id'] = $data['category'];
        $data['active'] = 1;

        if($this->model->create($data))
        {
            session()->flash('msg', 'Item cadastrado com sucesso');
            session()->flash('status', 'success');
            return redirect()->back();
        }else{
            session()->flash('msg', 'Erro ao adicionar Item');
            session()->flash('status', 'danger');
            return redirect()->back();
        }

    }
}
