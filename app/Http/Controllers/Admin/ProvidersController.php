<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Repositories\Contracts\ProvidersRepositoryInterface;
use App\Repositories\Contracts\UserRepositorieInterface;
use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProvidersController extends Controller
{

    protected $route = 'providers';
    protected $paginate = 5;
    protected $model = '';
    protected $model_user = '';
    protected $inactive_route = 'block_provider';

    public function __construct(ProvidersRepositoryInterface $model, UserRepositorieInterface $model_user)
    {
        $this->middleware('admin');
        $this->model = $model;
        $this->model_user = $model_user;

    }


    public function index(Request $request)
    {
        $columnList = [
            'name' => 'Nome',
            'email' => 'E-mail',
            'phone' => 'Telefone',
            'street' => 'Endereço',
            'active' => 'Ativo',
        ];

        $search = '';

        if(isset($request->search)){
            $search = $request->search;
            $list = $this->model->findWhereLikeProviders([
                'name',
                'email'
            ], $search, 'id', 'DESC');


            foreach ($list as $key => $item) {
                $list[$key]->active = $item->active ? "Sim" : 'Não';
            }


        }else{

            $list = $this->model->paginate($this->paginate, 'id', 'DESC');

            foreach ($list as $key => $item){
                $list[$key]->active = $item->user->active ? "Sim" : "Não";
                $list[$key]->name = $item->user->name;
                $list[$key]->email = $item->user->email;
            }
        }


        $routeName = $this->route;
        $inactiveRoute = $this->inactive_route;

        return view('admin.'.$routeName.'.index', compact('list', 'search', 'routeName', 'columnList', 'inactiveRoute'));

    }

    public function create()
    {
        $routeName = $this->route;

        return view('admin.'.$routeName.'.create', compact('routeName'));
    }

    public function edit($id)
    {

        $routeName = $this->route;
        $register = $this->model->find($id);

        if($register){
            return view('admin.'.$routeName.'.edit', compact('routeName', 'register'));
        }

        return redirect()->route($routeName.'.index');

    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data_provider = $this->model->find($id);

        if(!$data['password']){
            unset($data['password']);
        }

        Validator::make($data, [
            'name' => 'required|string|max:60',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($data_provider->user->id)],
            'password' => 'sometimes|required|string|min:3|confirmed',
            'phone' => 'required|string|max:20',
            'cep' => 'required|string|max:15',
            'number' => 'integer',
        ])->validate();

        if($data['password']){
            $data['password'] = Hash::make($data['password']);
        }

        if($this->model->update($data, $id))
        {
            session()->flash('msg', 'Fornecedor alterado com sucesso');
            session()->flash('status', 'success');
            return redirect()->back();
        }else{
            session()->flash('msg', 'Erro ao alterar Fornecedor');
            session()->flash('status', 'danger');
            return redirect()->back();
        }

    }

    public function inactive($id){

        $routeName = $this->route;

        $provider = $this->model->find($id);
        $data = $this->model_user->find($provider->user->id)->toArray();
        $msg = '';

        $data['active'] = $data['active'] ? 0 : 1;
        $msg = $data['active'] ? "ativado" : "desativado";

        if($this->model_user->block($data, $provider->user->id)){
            session()->flash('msg', 'Fornecedor '.$msg.' com sucesso');
            session()->flash('status', 'success');
        }else{
            session()->flash('msg', 'Erro ao desativar Fornecedor');
            session()->flash('status', 'danger');
        }

        return redirect()->route($routeName.'.index');
    }

    public function store(Request $request){


        $data = $request->all();

        Validator::make($data, [
            'phone' => 'required|string|max:20',
            'cep' => 'required|string|max:15',
            'number' => 'integer',
        ])->validate();

        $data_user = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' =>  Hash::make(123),
            'type' => 3,
            'active' => 1
        ];


        $user = $this->model_user->createUser($data_user);

        if($user){

            $data_providers = [
                'phone' => $data['phone'],
                'cep' => $data['cep'],
                'street' => $data['street'],
                'number' => $data['number'],
                'neighborhood' => $data['neighborhood'],
                'city' => $data['city'],
                'uf' => $data['uf'],
                'user_id' => $user->id,
            ];

            if($this->model->create($data_providers))
            {
                session()->flash('msg', 'Fornecedor cadastrado com sucesso');
                session()->flash('status', 'success');
                return redirect()->back();
            }else{
                session()->flash('msg', 'Erro ao adicionar Fornecedor');
                session()->flash('status', 'danger');
                return redirect()->back();
            }
        }else{
            session()->flash('msg', 'Erro ao adicionar Fornecedor');
            session()->flash('status', 'danger');
            return redirect()->back();
        }


    }
}
