<?php

namespace App\Http\Controllers\Admin;

use App\Clients;
use App\Collaborators;
use App\OrderService;
use App\Products;
use App\Providers;
use App\Repositories\Contracts\OrderServiceRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    protected $model = '';

    public function __construct(OrderServiceRepositoryInterface $model)
    {
        $this->middleware('admin');
        $this->model = $model;
    }

    public function index()
    {

        /*
         * Dados
         * Nando Marzola 18/02/2020
         */
        $total_clients = Clients::all();
        $total_providers = Providers::all();
        $total_coll = Collaborators::all();
        $products = Products::all();

        /*
         * Serviços
         * Nando Marzola 18/02/2020
         */
        $total_geral = OrderService::all();
        $open = $this->model->countOrders(1);
        $closed = $this->model->countOrders(2);

        return view('admin.dashboard.index', compact('total_geral', 'open', 'closed', 'total_clients', 'total_providers', 'total_coll', 'products'));
    }
}
