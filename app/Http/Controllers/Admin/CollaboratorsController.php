<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\CollaboratorsRepositoryInterface;
use App\Repositories\Contracts\UserRepositorieInterface;
use App\Services;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class CollaboratorsController extends Controller
{

    protected $route = 'collaborators';
    protected $paginate = 5;
    protected $model = '';
    protected $model_user = '';
    protected $inactive_route = 'block_collaborator';

    public function __construct(CollaboratorsRepositoryInterface $model, UserRepositorieInterface $model_user)
    {
        $this->middleware('admin');
        $this->model = $model;
        $this->model_user = $model_user;
    }


    public function index(Request $request)
    {

        $columnList = [
            'name' => 'Nome',
            'sex' => 'Sexo',
            'email' => 'E-mail',
            'function' => 'Função',
            'active' => 'Ativo',
        ];

        $search = "";

        $list_collaborators = $this->model->all();

        if(isset($request->search)){

            $search = $request->search;

            $list = $this->model->findWhereLikeCollaborator([
                'users.name',
            ], $search, 'id', 'DESC');


            foreach ($list as $key => $item) {
                $function = $this->model->collaboratorFunction($item->id);
                $list[$key]->sex = ($item->sex == 1) ? "Masculino" : 'Feminino';
                $list[$key]->function = $function;
                $list[$key]->active = $item->active ? "Sim" : 'Não';
            }

        }else{

            $list = $this->model->paginate($this->paginate, 'id', 'DESC');

            foreach ($list as $key => $item){
                $list[$key]->sex = ($item->sex == 1) ? "Masculino" : 'Feminino';
                $list[$key]->function = $item->service->name;
                $list[$key]->name = $item->user->name;
                $list[$key]->email = $item->user->email;
                $list[$key]->active = $item->user->active ? "Sim" : 'Não';;

            }
        }

        //echo $item->user->name.'-------id------'.$item->user->id.'-----idCOLLL-----'.$item->id;

        $routeName = $this->route;
        $inactiveRoute = $this->inactive_route;

        return view('admin.'.$routeName.'.index', compact('list', 'search', 'routeName', 'columnList', 'inactiveRoute'));
    }

    public function create(Request $request)
    {

        $data_services = Services::all();

        $routeName = $this->route;

        return view('admin.'.$routeName.'.create', compact('routeName', 'data_services'));
    }

    public function edit($id)
    {

        $routeName = $this->route;
        $register = $this->model->find($id);
        $data_services = Services::all();

        if($register){
            return view('admin.'.$routeName.'.edit', compact('routeName', 'register', 'data_services'));
        }

        return redirect()->route($routeName.'.index');

    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data_collaborators = $this->model->find($id);

        if(!$data['password']){
            unset($data['password']);
        }

        $data['service_id'] = $data['function'];

        Validator::make($data, [
            'name' => 'required|string|max:60',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($data_collaborators->user->id)],
            'password' => 'sometimes|required|string|min:3|confirmed',
            'sex' => 'required|boolean',
            'number' => 'integer',
            'cep' => 'required|string|max:35',

        ])->validate();

        if(isset($data['password'])){
            $data['password'] = Hash::make($data['password']);
        }


        if($this->model->update($data, $id))
        {
            session()->flash('msg', 'Colaborador alterado com sucesso');
            session()->flash('status', 'success');
            return redirect()->back();
        }else{
            session()->flash('msg', 'Erro ao alterar Colaborador');
            session()->flash('status', 'danger');
            return redirect()->back();
        }

    }

    public function inactive($id){

        $routeName = $this->route;

        $arrayClient = $this->model->find($id);
        $data = $this->model_user->find($arrayClient->user->id)->toArray();
        $msg = '';

        $data['active'] = $data['active'] ? 0 : 1;
        $msg = $data['active'] ? "ativado" : "desativado";

        if($this->model_user->block($data, $arrayClient->user->id)){
            session()->flash('msg', 'Colaborador '.$msg.' com sucesso');
            session()->flash('status', 'success');
        }else{
            session()->flash('msg', 'Erro ao desativar Colaborador');
            session()->flash('status', 'danger');
        }

        return redirect()->route($routeName.'.index');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        Validator::make($data, [
            'name' => 'required|string|max:60',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'number' => 'integer',
            'cep' => 'required|string|max:35',

        ])->validate();

        $data_user = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' =>  Hash::make(123),
            'sex' => 1,
            'type' => 2,
            'active' => 1
        ];

        


        $user = $this->model_user->createUser($data_user);

        if($user){

            $data_collaborators = [
                'service_id' => $data['function'],
                'sex' => 1,
                'cep' => $data['cep'],
                'street' => $data['street'],
                'number' => $data['number'],
                'neighborhood' => $data['neighborhood'],
                'city' => $data['city'],
                'uf' => $data['uf'],
                'user_id' => $user->id,
            ];

            if($this->model->create($data_collaborators))
            {
                session()->flash('msg', 'Colaborador cadastrado com sucesso');
                session()->flash('status', 'success');
                return redirect()->back();
            }else{
                session()->flash('msg', 'Erro ao adicionar Colaborador');
                session()->flash('status', 'danger');
                return redirect()->back();
            }
        }else{
            session()->flash('msg', 'Erro ao adicionar Colaborador');
            session()->flash('status', 'danger');
            return redirect()->back();
        }


    }
}
