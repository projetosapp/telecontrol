<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\ClientsRepositoryInterface;
use App\Repositories\Contracts\UserRepositorieInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ClientsController extends Controller
{

    protected $route = 'clients';
    protected $paginate = 10;
    protected $model = '';
    protected $inactive_route = 'block_client';

    public function __construct(ClientsRepositoryInterface $model, UserRepositorieInterface $model_user)
    {
        $this->middleware('admin');
        $this->model = $model;
    }

    public function index(Request $request)
    {
        $columnList = [
            'name' => 'Nome',
            'street' => 'Endereço',
            'city' => 'Cidade',
            'phone' => 'Telefone',
        ];

        $search = '';

        if(isset($request->search)){
            $search = $request->search;
            $list = $this->model->findWhereLike([
                'name',
            ], $search, 'id', 'DESC');

        }else{
            $list = $this->model->paginate($this->paginate, 'id', 'DESC');

        }


        $routeName = $this->route;

        return view('admin.'.$routeName.'.index', compact('routeName', 'list', 'search', 'columnList'));
    }

    public function create(Request $request)
    {

        $routeName = $this->route;

        return view('admin.'.$routeName.'.create', compact('routeName'));
    }

    public function edit($id)
    {

        $routeName = $this->route;
        $register = $this->model->find($id);

        if($register){
            return view('admin.'.$routeName.'.edit', compact('routeName', 'register'));
        }

        return redirect()->route($routeName.'.index');

    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        Validator::make($data, [
            'name' => 'required|string|max:60',
            'following' => 'required|string|max:30',
            'phone' => 'required|string',
            'number' => 'integer',
            'cep' => 'required|string|max:35',

        ])->validate();


        if($this->model->update($data, $id))
        {
            session()->flash('msg', 'Cliente alterado com sucesso');
            session()->flash('status', 'success');
            return redirect()->back();
        }else{
            session()->flash('msg', 'Erro ao alterar Cliente');
            session()->flash('status', 'danger');
            return redirect()->back();
        }

    }

    public function store(Request $request)
    {
        $data = $request->all();

        Validator::make($data, [
            'name' => 'required|string|max:60',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:clients'],
            'following' => 'required|string|max:30',
            'phone' => 'required|string',
            'number' => 'integer',
            'cep' => 'required|string|max:35',

        ])->validate();


        if($this->model->create($data))
        {
            session()->flash('msg', 'Cliente cadastrado com sucesso');
            session()->flash('status', 'success');
            return redirect()->back();
        }else{
            session()->flash('msg', 'Erro ao adicionar Cliente');
            session()->flash('status', 'danger');
            return redirect()->back();
        }

    }
}
