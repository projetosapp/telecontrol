<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\ServicesRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServicesController extends Controller
{

    protected $route = 'services';
    protected $paginate = 5;
    protected $model = '';
    protected $inactive_route = 'block_service';

    public function __construct(ServicesRepositoryInterface $model)
    {
        $this->middleware('admin');
        $this->model = $model;
    }


    public function index(Request $request)
    {
        $columnList = [
            'name' => 'Nome',
            'description' => 'Descrição',
            'value' => 'Valor da diária',
            'warranty' => 'Garantia',
            'active' => 'Ativo',
        ];

        $search = "";

        if(isset($request->search)){
            $search = $request->search;
            $list = $this->model->findWhereLike([
                'name',
                'description'
            ], $search, 'id', 'DESC');
        }else{
            $list = $this->model->paginate($this->paginate, 'id', 'DESC');
        }

        foreach ($list as $lista){
            $lista['value'] = "R$ ".number_format($lista['value'],2,",",".");
            $lista['active'] = $lista['active'] ? "Sim" : "Não";
            $lista['description'] = mb_strimwidth($lista['description'], 0, 60, "...");
            $lista['warranty'] = $lista['warranty_number'].' '.$lista['warranty_string'];
        }

        $routeName = $this->route;
        $inactiveRoute = $this->inactive_route;

        return view('admin.'.$routeName.'.index', compact('list', 'search', 'routeName', 'columnList', 'inactiveRoute'));
    }

    public function create()
    {
        $routeName = $this->route;

        return view('admin.services.create', compact('routeName'));
    }

    public function edit($id)
    {

        $routeName = $this->route;
        $register = $this->model->find($id);

        if($register){
            return view('admin.'.$routeName.'.edit', compact('routeName', 'register'));
        }

        return redirect()->route($routeName.'.index');

    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        Validator::make($data, [
            'name' => 'required|string|max:60',
            'diaria' => 'regex:/[\d]{2},[\d]{2}/',
            'description' => 'required', 'string', 'max:255',
        ])->validate();

        if($this->model->update($data, $id))
        {
            session()->flash('msg', 'Item alterado com sucesso');
            session()->flash('status', 'success');
            return redirect()->back();
        }else{
            session()->flash('msg', 'Erro ao alterar Item');
            session()->flash('status', 'danger');
            return redirect()->back();
        }

    }

    public function inactive($id){

        $routeName = $this->route;

        $data = $this->model->find($id)->toArray();
        $msg = '';

        $data['active'] = $data['active'] ? 0 : 1;
        $msg = $data['active'] ? "ativado" : "desativado";

        if($this->model->block($data, $id)){
            session()->flash('msg', 'Serviço '.$msg.' com sucesso');
            session()->flash('status', 'success');
        }else{
            session()->flash('msg', 'Erro ao '.$msg.' Serviço');
            session()->flash('status', 'danger');
        }

        return redirect()->route($routeName.'.index');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        Validator::make($data, [
            'name' => 'required|string|max:255',
            'diaria' => 'regex:/[\d]{2},[\d]{2}/',
            'description' => 'required|string|max:255',

        ])->validate();

        $data['diaria'] = str_replace(',', '.', $data['diaria']);
        $data['value'] = (float) $data['diaria'];
        $data['active'] = 1;

        if($this->model->create($data))
        {
            session()->flash('msg', 'Serviço cadastrado com sucesso');
            session()->flash('status', 'success');
            return redirect()->back();
        }else{
            session()->flash('msg', 'Erro ao adicionar serviço');
            session()->flash('status', 'danger');
            return redirect()->back();
        }

    }
}
