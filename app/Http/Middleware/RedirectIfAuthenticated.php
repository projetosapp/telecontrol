<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{

    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::guard($guard)->check()) {

            if(Auth::user()->type == 1){
                return redirect('admin/dashboard');
            }elseif(Auth::user()->type == 2){
                return redirect('collaborator/dashboard');
            }elseif(Auth::user()->type == 3){
                return redirect('provider/dashboard');
            }

            return redirect('/');
        }

        return $next($request);
    }
}
