<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if($user->type == 1){
            return $next($request);
        } else
            return redirect('/logout');

    }
}
