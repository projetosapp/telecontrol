<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Provider
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if($user->type == 3){
            return $next($request);
        } else
            return redirect('/logout');

    }
}
