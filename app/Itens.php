<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Itens extends Authenticatable
{

    protected $fillable = [
        'name', 'service_id', 'description', 'active'
    ];

    public function service()
    {
        return $this->belongsTo('App\Services');
    }
}
