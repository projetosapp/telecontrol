<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Collaborators extends Authenticatable
{


    protected $fillable = [
        'sex',
        'service_id',
        'cep',
        'street',
        'number',
        'neighborhood',
        'city',
        'uf',
        'type',
        'user_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function service()
    {
        return $this->belongsTo('App\Services');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
