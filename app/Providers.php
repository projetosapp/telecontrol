<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Providers extends Authenticatable
{
    protected $fillable = [

        'user_id',
        'phone',
        'cep',
        'street',
        'neighborhood',
        'uf',
        'number',
        'city',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
