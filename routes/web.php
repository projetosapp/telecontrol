<?php

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/home', 'Controller@redirectHome')->name('redirectHome');


/*
 * Rotas Method GET
 * Nando Marzola 15/02/2020
 */

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/login', 'Auth\LoginController@login')->name('logout_get');

/*
 * Rotas Method POST
 * Nando Marzola 15/02/2020
 */
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

/*
 * Rotas ADMINISTRADOR
 * NandoMarzola 15/02/2019
 */
Route::prefix('admin')->namespace('Admin')->group(function() {

    Route::resource('/dashboard', 'DashboardController');
    Route::get('/reports/{type}', 'ReportsController@index')->name('reports');
    Route::get('/reports/show/{id}', 'ReportsController@show')->name('reports_show_admin');
    Route::get('/ordem_de_servico/{id}', 'ReportsController@pdfService')->name('pdf-service-admin');

    Route::resource('/services', 'ServicesController');
    Route::resource('/products', 'ProductsController');
    Route::resource('/itens', 'ItensController');
    Route::resource('/clients', 'ClientsController');
    Route::resource('/collaborators', 'CollaboratorsController');
    Route::resource('/providers', 'ProvidersController');
    Route::resource('/order_service', 'OrderServiceController');
    
    /*
     * Rotas para desativar/ativar registros
     * Nando Marzola 15/02/2020
     */

    Route::get('/services/inactive/{id}', 'ServicesController@inactive')->name('block_service');
    Route::get('/products/inactive/{id}', 'ProductsController@inactive')->name('block_product');
    Route::get('/itens/inactive/{id}', 'ItensController@inactive')->name('block_item');
    Route::get('/clients/inactive/{id}', 'ClientsController@inactive')->name('block_client');
    Route::get('/collaborators/inactive/{id}', 'CollaboratorsController@inactive')->name('block_collaborator');
    Route::get('/providers/inactive/{id}', 'ProvidersController@inactive')->name('block_provider');

    /*
     * Rota para exportar/importar produto
     */

    Route::get('/products/export/{name}', 'ProductsController@export')->name('export_products');
    Route::post('/products/import/', 'ProductsController@import')->name('import_products');

});


/*
 * Rotas COLABORADORES
 * NandoMarzola 15/02/2019
 */
Route::prefix('collaborator')->namespace('Collaborator')->group(function() {

    Route::get('/dashboard', 'DashboardController@index')->name('home');
    Route::get('/reports/{type}', 'ReportsController@index')->name('reports_coll');
    Route::get('/reports/show/{id}', 'ReportsController@show')->name('reports_show_coll');

    Route::post('/client-json', 'OrderServiceController@returnClientJson')->name('client-json');
    Route::post('/product-json', 'OrderServiceController@returnProductJson')->name('product-json');

    Route::get('/ordem_de_servico/{id}', 'OrderServiceController@pdfService')->name('pdf-service');

});

/*
 * Rotas FORNECEDORES
 * NandoMarzola 15/02/2019
 */
Route::prefix('provider')->namespace('Provider')->group(function() {

    Route::resource('/dashboard', 'DashboardController');

});

